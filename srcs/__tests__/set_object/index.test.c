#include "rt.test.h"

void		test_set_object(void)
{
	RUN_TEST_GROUP(set_sphere);
	RUN_TEST_GROUP(set_cone);
	RUN_TEST_GROUP(set_cyl);
	RUN_TEST_GROUP(set_plane);
	RUN_TEST_GROUP(set_rectangle);
	RUN_TEST_GROUP(set_box);
	RUN_TEST_GROUP(set_pyramid);
	RUN_TEST_GROUP(set_ring);
	RUN_TEST_GROUP(set_triangle);
}
